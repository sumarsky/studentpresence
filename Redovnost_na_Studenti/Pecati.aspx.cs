﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.OleDb;
using System.Configuration;
using System.Drawing;
using System.Windows.Forms;



namespace Redovnost_na_Studenti
{
    public partial class Pecati : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            string imePrezime = Session["imePrezime"].ToString();
            Label1.Text = imePrezime;
            int idProfessor = Convert.ToInt32(Session["id"].ToString());

            string kursParametarId = Session["kursParametar"].ToString();
            string data = Session["izbranaData"].ToString();

            string myConnection = ConfigurationManager.ConnectionStrings["konekcija"].ConnectionString;
            OleDbConnection conn = new OleDbConnection(myConnection);
            conn.Open();

            string imeKurs = "select kursIme from COURSE where ID = " + kursParametarId;
            OleDbCommand cmd1 = new OleDbCommand(imeKurs, conn);
            Object kIme = cmd1.ExecuteScalar();

            conn.Close();
            Label2.Text = kIme.ToString();
            Label3.Text = "Дата:" + data;

        }

    }
}