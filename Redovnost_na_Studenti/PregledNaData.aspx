﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PregledNaData.aspx.cs" Inherits="Redovnost_na_Studenti.PregledNaData" %>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Курсеви</title>
    <LINK href="Resources/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div align="center">
         <div class="headerDiv" align="center">
            <div class="headerInsideDiv">
                <img src="/Resources/Logo_FINKI_MK.jpg" style="height:50px;float:left"/>
                <div style="padding-top:14px;font-family: Arial, Helvetica, sans-serif; text-align:right;">
                    Најавен сте како: <asp:Label ID="Label1" runat="server" Text="" ForeColor="#0033CC"></asp:Label><!-- Ime Prezime -->
                    (<a href="Najava.aspx">Одјави се</a>)&nbsp&nbsp&nbsp&nbsp
                </div>
            </div>
        </div>

        

       <div class="bodyDiv">
        <br/>
            <a href="KurseviP.aspx" style=" text-decoration:none; cursor:pointer;"><asp:Label ID="Label5" class="labeli" runat="server" Text="Мои курсеви" style="float:left; margin-left:20px;" ForeColor="#2E92D0"></asp:Label></a>
            <asp:Image ID="Image1" runat="server" ImageUrl="~/Resources/triagolnik.png" Height="10px" Width="10px" style="float:left; margin-left:5px; margin-top:5px" />
            <a href="TokeniZaKurs.aspx" style=" text-decoration:none; cursor:pointer;"><asp:Label ID="Label2" runat="server" Text="" class="labeli" style="float:left; margin-left:5px;" ForeColor="#2E92D0" ></asp:Label></a>
            <asp:Image ID="Image2" runat="server" ImageUrl="~/Resources/triagolnik.png" Height="10px" Width="10px" style="float:left; margin-left:5px; margin-top:5px" />
            <asp:Label ID="Label3" runat="server" Text="" class="labeli" style="float:left; margin-left:5px;" ></asp:Label>
           
            <br/>
            <div style="float:left; margin-left:50px; margin-top:40px; ">
                <asp:GridView ID="GridView1" runat="server" Width="280px" 
                     Font-Size="18px" 
                    HeaderStyle-BackColor="#2E92D0" RowStyle-Height="30px" 
                    RowStyle-HorizontalAlign="Left" HeaderStyle-Height="30px" BackColor="#DDDDDD" 
                    AutoGenerateColumns="False" DataSourceID="AccessDataSource1" 
                    EnableModelValidation="True">
                    <Columns>
                        <asp:BoundField DataField="ime" HeaderText="Име" SortExpression="ime" />
                        <asp:BoundField DataField="prezime" HeaderText="Презиме" 
                            SortExpression="prezime" />
                        <asp:BoundField DataField="kluc" HeaderText="Клуч" SortExpression="kluc" />
                    </Columns>
                    <HeaderStyle BackColor="#2E92D0" Height="30px" />
                    <RowStyle Height="30px" HorizontalAlign="Left" />
                </asp:GridView>
                <asp:AccessDataSource ID="AccessDataSource1" runat="server" 
                    DataFile="/App_Data/Redovnost.mdb" 
                    SelectCommand="SELECT  s.ime AS ime, s.prezime AS prezime, t.kluc AS kluc
                                    FROM TOKEN t, STUDENT s 
                                    WHERE t.data = sesData and
                                    t.courseId=sesKursId and
                                    t.studentId=s.ID
                                    
                                    UNION

                                    SELECT  s.ime AS ime, s.prezime AS prezime, ' / ' AS kluc
                                    FROM STUDENT s, STUDENT_COURSE k, COURSE c
                                    WHERE 
                                    s.ID = k.studentId and
                                    c.ID = k.courseId and
                                    c.ID=sesKursId and
                                    s.ID not in 
                                    (SELECT  s.ID
                                    FROM TOKEN t, STUDENT s 
                                    WHERE t.data = sesData and
                                    t.courseId=sesKursId and
                                    t.studentId=s.ID) ">
                    <SelectParameters>
                        <asp:SessionParameter Name="sesData" SessionField="izbranaData" />
                        <asp:SessionParameter Name="sesKursId" SessionField="kursParametar" /> 
                    </SelectParameters>
                </asp:AccessDataSource>
            </div>
    </div>
    </form>
</body>
</html>

