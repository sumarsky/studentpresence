﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Pecati.aspx.cs" Inherits="Redovnost_na_Studenti.Pecati" %>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Курсеви</title>
    <LINK href="Resources/style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function CallPrint(strid) {
            var prtContent = document.getElementById(strid);
            var WinPrint = window.open('', '', 'letf=0,top=0,toolbar=0,scrollbars=0,sta¬tus=0');
            WinPrint.document.write(prtContent.innerHTML);
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();
            prtContent.innerHTML = strOldOne;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div align="center">
         <div class="headerDiv" align="center">
            <div class="headerInsideDiv">
                <img src="/Resources/Logo_FINKI_MK.jpg" style="height:50px;float:left"/>
                <div style="padding-top:14px;font-family: Arial, Helvetica, sans-serif; text-align:right;">
                    Најавен сте како: <asp:Label ID="Label1" runat="server" Text="" ForeColor="#0033CC"></asp:Label><!-- Ime Prezime -->
                    (<a href="Najava.aspx">Одјави се</a>)&nbsp&nbsp&nbsp&nbsp
                </div>
            </div>
        </div>

        

       <div class="bodyDiv">
        <br/>
            <a href="KurseviP.aspx" style=" text-decoration:none; cursor:pointer;"><asp:Label ID="Label5" class="labeli" runat="server" Text="Мои курсеви" style="float:left; margin-left:20px;" ForeColor="#2E92D0"></asp:Label></a>
            <asp:Image ID="Image1" runat="server" ImageUrl="~/Resources/triagolnik.png" Height="10px" Width="10px" style="float:left; margin-left:5px; margin-top:5px" />
            <a href="TokeniZaKurs.aspx" style=" text-decoration:none; cursor:pointer;"><asp:Label ID="Label2" runat="server" Text="" class="labeli" style="float:left; margin-left:5px;" ForeColor="#2E92D0" ></asp:Label></a>
            <asp:Image ID="Image2" runat="server" ImageUrl="~/Resources/triagolnik.png" Height="10px" Width="10px" style="float:left; margin-left:5px; margin-top:5px" />
            <asp:Label ID="Label3" runat="server" Text="" class="labeli" style="float:left; margin-left:5px;" ></asp:Label>
           
            <br/>
            <div>
                <div id="divPrint" style="float:left; margin-left:50px; margin-top:40px; ">
                    <asp:GridView ID="GridView1" runat="server" Width="600px" 
                         Font-Size="18px" 
                        HeaderStyle-BackColor="#2E92D0" RowStyle-Height="30px" 
                        RowStyle-HorizontalAlign="Left" HeaderStyle-Height="30px" BackColor="#DDDDDD" 
                        AutoGenerateColumns="False" DataSourceID="AccessDataSource1" 
                        EnableModelValidation="True">
                        <Columns>
                            <asp:TemplateField HeaderText="Ред.бр"  HeaderStyle-Width="10%">
                            <ItemTemplate>    
                               <%# ((GridViewRow)Container).RowIndex + 1%>
                           </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="kluc" HeaderText="Токен" SortExpression="kluc" 
                                HeaderStyle-Width="30%" >
    <HeaderStyle Width="30%"></HeaderStyle>
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Име и презиме" HeaderStyle-Width="60%">
    <HeaderStyle Width="70%"></HeaderStyle>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle BackColor="#2E92D0" Height="30px" />
                        <RowStyle Height="30px" HorizontalAlign="Left" />
                    </asp:GridView>
                </div>
                <asp:AccessDataSource ID="AccessDataSource1" runat="server" 
                    DataFile="/App_Data/Redovnost.mdb" 
                    
                    SelectCommand="SELECT [kluc] FROM [TOKEN] WHERE (([courseId] = ?) AND ([data] = ?) AND ([aktiven] = ?))">
                    <SelectParameters>
                        <asp:SessionParameter Name="courseId" SessionField="kursParametar" Type="Int32" />
                        <asp:SessionParameter Name="data" SessionField="izbranaData" Type="DateTime" /> 
                        <asp:Parameter DefaultValue="true" Name="aktiven" Type="Boolean" />
                    </SelectParameters>
                </asp:AccessDataSource>

                <asp:Button ID="btnPrint" runat="server" Text="Печати" Font-Bold="True" 
                   ForeColor="#000099" Height="25px" Width="60px" 
                   style="float:right; margin-right:150px; margin-top:40px;" 
                   OnClientClick="javascript:CallPrint('divPrint');" />
            </div>
        </div>
    </form>
</body>
</html>

