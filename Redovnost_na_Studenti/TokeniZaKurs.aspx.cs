﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.OleDb;
using System.Data;

namespace Redovnost_na_Studenti
{
    public partial class TokeniZaKurs : System.Web.UI.Page
    {
        //private Object brStudenti;
        static Random _random = new Random();
        public static char GetLetter()
        {
            // This method returns a random lowercase letter.
            // ... Between 'a' and 'z' inclusize.
            int num = _random.Next(0, 26); // Zero to 25
            char let = (char)('A' + num);
            return let;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string imePrezime = Session["imePrezime"].ToString();
            Label1.Text = imePrezime;
            int idProfessor = Convert.ToInt32(Session["id"].ToString());

            string kursParametarId = Session["kursParametar"].ToString();

            string myConnection = ConfigurationManager.ConnectionStrings["konekcija"].ConnectionString;
            OleDbConnection conn = new OleDbConnection(myConnection);
            conn.Open();

            string imeKurs = "select kursIme from COURSE where ID = " + kursParametarId;
            OleDbCommand cmd1 = new OleDbCommand(imeKurs, conn);
            Object kIme = cmd1.ExecuteScalar();
            
            conn.Close();
            Label2.Text = kIme.ToString();
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            GridViewRow gv = (GridViewRow)(((Control)sender).NamingContainer);
            Label lbid = (Label)gv.FindControl("Label1");
            String parametar = lbid.Text;
            DateTime tmp = DateTime.Parse(parametar);
            Session["izbranaData"] = tmp.ToString("dd.MM.yyyy");
            Response.Redirect("TokeniZaData.aspx");
        }

        protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
        {
            GridViewRow gv = (GridViewRow)(((Control)sender).NamingContainer);
            Label lbid = (Label)gv.FindControl("Label1");
            String parametar = lbid.Text;
            DateTime tmp = DateTime.Parse(parametar);
            Session["izbranaData"] = tmp.ToString("dd.MM.yyyy");
            Response.Redirect("PregledNaData.aspx");
        }

        protected void ImageButton3_Click(object sender, ImageClickEventArgs e)
        {
            GridViewRow gv = (GridViewRow)(((Control)sender).NamingContainer);
            Label lbid = (Label)gv.FindControl("Label1");
            String parametar = lbid.Text;
            Session["izbranaData"] = parametar;
            AccessDataSource1.Delete();
        }

        protected void ImageButton4_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Statistika.aspx");
        }

        protected void ImageButton5_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Statistiki1.aspx");
        }

        protected void ImageButton6_Click(object sender, ImageClickEventArgs e)
        {
            GridViewRow gv = (GridViewRow)(((Control)sender).NamingContainer);
            Label lbid = (Label)gv.FindControl("Label1");
            String parametar = lbid.Text;
            DateTime tmp = DateTime.Parse(parametar);
            Session["izbranaData"] = tmp.ToString("dd.MM.yyyy");
            Response.Redirect("Pecati.aspx");
        }
        protected void Generate_Click(object sender, EventArgs e)
        {
            //Funkcija koja treba da se implementira za generiranje tokeni i update vo bazata
            //datata se zema od Labela so ID = VnesiData

            char[] data = VnesiData.Text.ToCharArray();

            DataView dv = (DataView)AccessDataSource2.Select(DataSourceSelectArguments.Empty);
            int a = (int)dv.Table.Rows[0][0];

            for (int i = 0; i < a; i++)
            {
                string token = "";
                token += data[0].ToString() + data[1].ToString();
                token += data[3].ToString() + data[4].ToString();
                token += data[8].ToString() + data[9].ToString();

                for (int j = 0; j < 5; j++)
                    token += GetLetter();

                Session["token"] = token;
                AccessDataSource2.Insert();
            }

            GridView1.DataBind();
            VnesiData.Text = "";
        }


    }
}