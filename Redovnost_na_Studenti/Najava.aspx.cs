﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.OleDb;

namespace Redovnost_na_Studenti
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session.Clear();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string myConnection = ConfigurationManager.ConnectionStrings["konekcija"].ConnectionString;
            OleDbConnection conn = new OleDbConnection(myConnection);
            conn.Open();

            string zemiUsername = "select username from STUDENT where Username = '" + TextBox1.Text + "'";
            OleDbCommand cmd1 = new OleDbCommand(zemiUsername, conn);
            Object kIme = cmd1.ExecuteScalar();

            if (kIme != null)
            {
                string korisnickoIme = kIme.ToString();
                string zemiPass = "select password, ime, prezime, id from STUDENT where Username = '" + korisnickoIme + "'";
                OleDbCommand cmd2 = new OleDbCommand(zemiPass, conn);
                OleDbDataReader r = cmd2.ExecuteReader();
                r.Read();
                String password = r["password"].ToString();
                String ime = r["ime"].ToString();
                String prezime = r["prezime"].ToString();
                int id = Convert.ToInt32(r["id"].ToString());
                r.Close();

                if (TextBox2.Text == password)
                {
                    Session["imePrezime"] = "" + ime + " " + prezime;
                    Session["id"] = id;
                    Response.Redirect("KurseviS.aspx");
                }
                else
                    Label3.Text = "Внесовте неточна лозинка";
            }
            else
            {
                zemiUsername = "select username from PROFESSOR where Username = '" + TextBox1.Text + "'";
                cmd1 = new OleDbCommand(zemiUsername, conn);
                kIme = cmd1.ExecuteScalar();

                if (kIme != null)
                {
                    string korisnickoIme = kIme.ToString();
                    string zemiPass = "select password, ime, prezime, id from PROFESSOR where Username = '" + korisnickoIme + "'";
                    OleDbCommand cmd2 = new OleDbCommand(zemiPass, conn);
                    OleDbDataReader r = cmd2.ExecuteReader();
                    r.Read();
                    String password = r["password"].ToString();
                    String ime = r["ime"].ToString();
                    String prezime = r["prezime"].ToString();
                    int id = Convert.ToInt32(r["id"].ToString());
                    r.Close();

                    if (TextBox2.Text == password)
                    {
                        Session["imePrezime"] = "" + ime + " " + prezime;
                        Session["id"] = id;
                        Response.Redirect("KurseviP.aspx");
                    }
                    else
                        Label3.Text = "Внесовте неточна лозинка";
                }
                else
                    Label3.Text = "Внесовте непостоечко корисничко име";
            }

            conn.Close();
        }
    }
}