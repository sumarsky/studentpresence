﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="KurseviS.aspx.cs" Inherits="Redovnost_na_Studenti.KurseviS" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Курсеви</title>
    <LINK href="Resources/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div align="center">
        <div class="headerDiv" align="center">
            <div class="headerInsideDiv">
                <img src="/Resources/Logo_FINKI_MK.jpg" style="height:50px;float:left"/>
                <div style="padding-top:14px;font-family: Arial, Helvetica, sans-serif; text-align:right;">
                    Најавен сте како: <asp:Label ID="Label1" runat="server" Text="" ForeColor="#0033CC"></asp:Label><!-- Ime Prezime -->
                    (<a href="Najava.aspx">Одјави се</a>)&nbsp&nbsp&nbsp&nbsp
                </div>
            </div>
        </div>
        
       

        <div class="bodyDiv">
        <br/>
        <asp:Label ID="Label3" class="labeli" runat="server" Text="Мои курсеви" style="float:left; margin-left:20px;" ForeColor="#2E92D0"></asp:Label>
        <br/>
            <div style="float:left; margin-left:50px; margin-top:30px; ">
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                    DataKeyNames="ID" DataSourceID="AccessDataSource1" 
                    EnableModelValidation="True" ShowHeader="False" 
                    Font-Italic="False" Font-Size="18px" Font-Bold="True" Width="400px" ForeColor="Black" GridLines="None" RowStyle-Height="40px">
                    <Columns>
                        <asp:TemplateField HeaderText="kursIme" SortExpression="kursIme">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("kursIme") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                            <asp:Image ID="Image1" runat="server" Height="10px" 
                                     ImageUrl="~/Resources/triagolnik.png" Width="10px" style="margin-top:5px"/>
                                 &nbsp;
                                <asp:Button ID="Button1" runat="server" BorderStyle="None" 
                                    onclick="Button1_Click" Text='<%# Bind("kursIme") %>' BackColor="#DDDDDD" 
                                    BorderColor="#DDDDDD" Font-Bold="True" Font-Size="18px" style="cursor:pointer;"  />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ID" InsertVisible="False" SortExpression="ID" 
                            Visible="False">
                            <EditItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <asp:AccessDataSource ID="AccessDataSource1" runat="server" 
                    DataFile="/App_Data/Redovnost.mdb" 
                    
                    SelectCommand="SELECT COURSE.kursIme, COURSE.ID FROM ((COURSE INNER JOIN STUDENT_COURSE ON COURSE.ID = STUDENT_COURSE.courseId) INNER JOIN STUDENT ON STUDENT_COURSE.studentId = STUDENT.ID) WHERE (STUDENT.ID = ?)">
                    <SelectParameters>
                        <asp:SessionParameter Name="?" SessionField="id" />
                    </SelectParameters>
                </asp:AccessDataSource>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
