﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Statistiki1.aspx.cs" Inherits="Redovnost_na_Studenti.Statistiki1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Курсеви</title>
    <LINK href="Resources/style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div align="center">
         <div class="headerDiv" align="center">
            <div class="headerInsideDiv">
                <img src="/Resources/Logo_FINKI_MK.jpg" style="height:50px;float:left"/>
                <div style="padding-top:14px;font-family: Arial, Helvetica, sans-serif; text-align:right;">
                    Најавен сте како: <asp:Label ID="Label1" runat="server" Text="" ForeColor="#0033CC"></asp:Label><!-- Ime Prezime -->
                    (<a href="Najava.aspx">Одјави се</a>)&nbsp&nbsp&nbsp&nbsp
                </div>
            </div>
        </div>



        <div class="bodyDiv">
        <br/>
        <a href="KurseviP.aspx" style=" text-decoration:none; cursor:pointer;"><asp:Label ID="Label5" class="labeli" runat="server" Text="Мои курсеви" style="float:left; margin-left:20px;" ForeColor="#2E92D0"></asp:Label></a>
            <asp:Image ID="Image1" runat="server" ImageUrl="~/Resources/triagolnik.png" Height="10px" Width="10px" style="float:left; margin-left:5px; margin-top:5px" />
            <a href="TokeniZaKurs.aspx" style=" text-decoration:none; cursor:pointer;"><asp:Label ID="Label2" runat="server" Text="" class="labeli" style="float:left; margin-left:5px;" ForeColor="#2E92D0" ></asp:Label></a>
            <asp:Image ID="Image2" runat="server" ImageUrl="~/Resources/triagolnik.png" Height="10px" Width="10px" style="float:left; margin-left:5px; margin-top:5px" />
            <asp:Label ID="Label3" runat="server" Text="" class="labeli" style="float:left; margin-left:5px;" ></asp:Label>
            <div id="dijagram" style="margin-top:40px; width:95%">
            
            </div>
        </div>
    </form>
</body>
</html>
