﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.OleDb;
using System.Configuration;
using System.Collections;

namespace Redovnost_na_Studenti
{
    public partial class Statistika : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string imePrezime = Session["imePrezime"].ToString();
            Label1.Text = imePrezime;
            int idProfessor = Convert.ToInt32(Session["id"].ToString());

            string kursParametarId = Session["kursParametar"].ToString();

            string myConnection = ConfigurationManager.ConnectionStrings["konekcija"].ConnectionString;
            OleDbConnection conn = new OleDbConnection(myConnection);
            conn.Open();

            string imeKurs = "select kursIme from COURSE where ID = " + kursParametarId;
            OleDbCommand cmd1 = new OleDbCommand(imeKurs, conn);
            Object kIme = cmd1.ExecuteScalar();

            conn.Close();
            Label2.Text = kIme.ToString();
            Label3.Text = "Статистички податоци за присуство";

            ArrayList grafData = new ArrayList();
            ArrayList grafBroj = new ArrayList();

            conn.Open();
            string grafik = "Select data, Count(*) AS broj From Token Where token.studentId IS NOT NULL AND token.courseId = "+kursParametarId+" Group by data";
            OleDbCommand cmd2 = new OleDbCommand(grafik, conn);

            OleDbDataReader r = cmd2.ExecuteReader();
            r.Read();
            if (r.HasRows)
            {
                DateTime tmp = DateTime.Parse(r["data"].ToString());
                String pom = tmp.ToString("dd.MM.yyyy");
                grafData.Add(pom.ToString());
                grafBroj.Add(r["broj"].ToString());

                while (r.Read())
                {
                    DateTime tmp1 = DateTime.Parse(r["data"].ToString());
                    String pom1 = tmp1.ToString("dd.MM.yyyy");
                    grafData.Add(pom1);
                    grafBroj.Add(r["broj"].ToString());

                }
            }

            conn.Close();
            
            string javaScript =
               "<script type='text/javascript'> "+
                      "google.load('visualization', '1', {packages:['corechart']}); "+
                      "google.setOnLoadCallback(drawChart); "+
                      "function drawChart() { "+
                       " var data = new google.visualization.DataTable(); "+
                        "data.addColumn('string', 'Дата'); "+
                        "data.addColumn('number', 'Број на студенти'); "+

                        "data.addRows([ ";
                          for(int i =0; i<grafData.Count; i++){ 
                              javaScript+="['"+grafData[i]+"',"+grafBroj[i]+"]";
                              if(i!=grafData.Count-1)
                                  javaScript+=",";
                          } 
                        javaScript+="]); "+

                        "var options = { "+
                         " title: 'Број на студенти по дати', "+
                          "hAxis: {title: 'Дата', titleTextStyle: {color: 'blue'}} "+
                       " }; "+

                        "var chart = new google.visualization.ColumnChart(document.getElementById('dijagram')); " +
                        "chart.draw(data, options); "+
                     " } "+
            "</script>";

            ClientScript.RegisterStartupScript(this.GetType(), "PageLoad", javaScript);

        }
    }
}