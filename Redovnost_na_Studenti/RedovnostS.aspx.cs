﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.OleDb;


namespace Redovnost_na_Studenti
{
    public partial class RedovnostS : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string imePrezime = Session["imePrezime"].ToString();
            Label1.Text = imePrezime;
            int idStudent = Convert.ToInt32(Session["id"].ToString());

            string kursParametarId = Session["kursParametar"].ToString();

            string myConnection = ConfigurationManager.ConnectionStrings["konekcija"].ConnectionString;
            OleDbConnection conn = new OleDbConnection(myConnection);
            conn.Open();

            string imeKurs = "select kursIme from COURSE where ID = " + kursParametarId;
            OleDbCommand cmd1 = new OleDbCommand(imeKurs, conn);
            Object kIme = cmd1.ExecuteScalar();

            string zemiDataNova = "select max(data) from TOKEN, COURSE where course.id = token.courseID and token.courseID = " + kursParametarId;
            OleDbCommand cmd2 = new OleDbCommand(zemiDataNova, conn);
            Object kData = cmd2.ExecuteScalar();

            if (kData.ToString() != "")
            {
                DateTime tmp = (DateTime)kData;
                if (DateTime.Now > tmp && DateTime.Now < tmp.AddDays(1))
                {
                    Label3.Visible = true;
                    TextBox1.Visible = true;
                    Button1.Visible = true;

                    DateTime labelaData = DateTime.Parse(kData.ToString());
                    Label3.Text = labelaData.ToString("dd.MM.yyyy");
                }
                else
                {
                    Label3.Visible = false;
                    TextBox1.Visible = false;
                    Button1.Visible = false;
                }
            }
            conn.Close();
            Label2.Text = kIme.ToString();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            int idStudent = Convert.ToInt32(Session["id"].ToString());

            string myConnection = ConfigurationManager.ConnectionStrings["konekcija"].ConnectionString;
            OleDbConnection conn = new OleDbConnection(myConnection);
            conn.Open();

            string zemiToken = "select token.kluc from COURSE, TOKEN where COURSE.id = TOKEN.courseid and token.kluc='"+TextBox1.Text+"'";
            OleDbCommand cmd1 = new OleDbCommand(zemiToken, conn);
            Object kTokenKluc = cmd1.ExecuteScalar();

            if (kTokenKluc != null)
            {
                string aktivenT = "select token.aktiven from COURSE, TOKEN where COURSE.id = TOKEN.courseid and token.kluc='" + kTokenKluc.ToString() + "'";
                OleDbCommand cmd2 = new OleDbCommand(aktivenT, conn);
                Object kAktivenT = cmd2.ExecuteScalar();

                if ((bool)kAktivenT)
                {
                    string updateTokens = "UPDATE TOKEN set aktiven=false, studentId = " + idStudent.ToString() + " where kluc = '" + kTokenKluc.ToString() + "'";
                    OleDbCommand cmd3 = new OleDbCommand(updateTokens, conn);
                    cmd3.ExecuteNonQuery();

                    Response.Write("<script>alert('Вашето присуство е потврдено')</script>");

                    Label3.Visible = false;
                    TextBox1.Visible = false;
                    Button1.Visible = false;
                    Label4.Text = "";
                }
                else
                {
                    Label4.Text = "Вашиот токен не е активен!";
                }
            }
            else
            {
                Label4.Text = "Внесовте непостоечки токен";
            }

            conn.Close();
            GridView1.DataBind();
            TextBox1.Text = "";
        }
    }
}