﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RedovnostS.aspx.cs" Inherits="Redovnost_na_Studenti.RedovnostS" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Курсеви</title>
    <LINK href="Resources/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div align="center">
         <div class="headerDiv" align="center">
            <div class="headerInsideDiv">
                <img src="/Resources/Logo_FINKI_MK.jpg" style="height:50px;float:left"/>
                <div style="padding-top:14px;font-family: Arial, Helvetica, sans-serif; text-align:right;">
                    Најавен сте како: <asp:Label ID="Label1" runat="server" Text="" ForeColor="#0033CC"></asp:Label><!-- Ime Prezime -->
                    (<a href="Najava.aspx">Одјави се</a>)&nbsp&nbsp&nbsp&nbsp
                </div>
            </div>
        </div>
        
        

        <div class="bodyDiv">
        <br/>
            <a href="KurseviS.aspx" style=" text-decoration:none; cursor:pointer;"><asp:Label ID="Label5" class="labeli" runat="server" Text="Мои курсеви" style="float:left; margin-left:20px;" ForeColor="#2E92D0"></asp:Label></a>
            <asp:Image ID="Image1" runat="server" ImageUrl="~/Resources/triagolnik.png" Height="10px" Width="10px" style="float:left; margin-left:5px; margin-top:5px" />
            <asp:Label ID="Label2" runat="server" Text="" class="labeli" style="float:left; margin-left:5px;"></asp:Label>
           
            <br/>
            <div style="float:left; margin-left:50px; margin-top:40px; ">
                <asp:GridView ID="GridView1" runat="server" DataSourceID="AccessDataSource1" 
                    EnableModelValidation="True" AutoGenerateColumns="False" Width="322px" 
                     Font-Size="18px" ForeColor="Black" 
                    HeaderStyle-BackColor="#2E92D0" RowStyle-Height="30px" HeaderStyle-Height="30px" BackColor="#DDDDDD">
                    <Columns>
                        <asp:TemplateField HeaderText="Датум" SortExpression="data">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("data") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("data") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Токен" SortExpression="kluc">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("kluc") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text='<%# Bind("kluc") %>'></asp:Label>
                            </ItemTemplate>
                            <FooterStyle BackColor="#2E92D0" />
                        </asp:TemplateField>
                    </Columns>

<HeaderStyle BackColor="#2E92D0"></HeaderStyle>
                </asp:GridView>
                <asp:AccessDataSource ID="AccessDataSource1" runat="server" 
                    DataFile="/App_Data/Redovnost.mdb" 
                    
                    
                    SelectCommand="SELECT t.data, t.kluc FROM ((STUDENT s INNER JOIN TOKEN t ON s.ID = t.studentId) INNER JOIN COURSE c ON t.courseId = c.ID) WHERE (s.ID = sID) AND (c.ID = cID) UNION SELECT t.data, ' ' AS Expr1 FROM (COURSE c INNER JOIN TOKEN t ON c.ID = t.courseId) WHERE (c.ID = cID) AND (t.data NOT IN (SELECT t.data FROM STUDENT s, TOKEN t, COURSE c WHERE (s.ID = t.studentId) AND (c.ID = t.courseId) AND (s.ID = sID) AND (c.ID = cID))) GROUP BY t.data ORDER BY t.data asc">
                    <SelectParameters>
                        <asp:SessionParameter Name="sID" SessionField="id" />
                        <asp:SessionParameter Name="cID" SessionField="kursParametar" />
                    </SelectParameters>
                </asp:AccessDataSource>

                <br/>
                <asp:Label ID="Label3" class="labeli" runat="server" Visible="False" 
                    style="float:left; " Font-Size="18px" BackColor="#DDDDDD"></asp:Label>
                <asp:TextBox ID="TextBox1" runat="server" Visible="False" style="float:left; margin-left:5px;"></asp:TextBox>
                <asp:Button ID="Button1" runat="server" class="kopce" Text="Внеси" onclick="Button1_Click" Visible="False" style="float:left; margin-left:5px; width:70px" />
                   
                <br/><br/>
                <asp:Label ID="Label4" runat="server" style="float:left;" Font-Size="16px"></asp:Label>

            </div>


        </div>
    </div>
    </form>
</body>
</html>
