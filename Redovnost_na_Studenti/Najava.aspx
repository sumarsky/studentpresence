﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Najava.aspx.cs" Inherits="Redovnost_na_Studenti._Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>СИСТЕМ ЗА РЕДОВНОСТ</title>
    <LINK href="Resources/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server" defaultfocus="TextBox1">
    <div align="center">

        <div class="headerDiv" align="center">
            <div class="headerInsideDiv">
                <img src="/Resources/Logo_FINKI_MK.jpg" style="height:50px;float:left"/>
                <div style="padding-top:10px">
                    <font class="naslovFont">СИСТЕМ ЗА ЕВИДЕНЦИЈА НА РЕДОВНОСТ</font>
                </div>
            </div>
        </div>

        

        <div class="bodyDiv">
            <div style="position:absolute; left:70px; top:150px; text-align:left">
                <asp:Label ID="Label1" class="labeli" runat="server" Text="Корисничко име" Width="120px"></asp:Label>
                <asp:TextBox ID="TextBox1" class="textBox" runat="server" Width="150px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    ErrorMessage="Внесете корисничко име" ControlToValidate="TextBox1" 
                    ValidationGroup="1" ForeColor="Blue"></asp:RequiredFieldValidator>
                <br />
                <asp:Label ID="Label2" class="labeli" runat="server" Text="Лозинка" Width="120px"></asp:Label>
                <asp:TextBox ID="TextBox2" class="textBox" runat="server" TextMode="Password" Width="150px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    ErrorMessage="Внесете лозинка" ControlToValidate="TextBox2" ValidationGroup="1" 
                    ForeColor="Blue"></asp:RequiredFieldValidator>
                <br />
                <br />
                
                <asp:Button ID="Button1" class="kopce" runat="server" Text="Најави се" ValidationGroup="1" 
                    onclick="Button1_Click" Width="79px" />
                <br />
                <br />
                <asp:Label ID="Label3" runat="server" ForeColor="Blue"></asp:Label>
            </div>
            <div style="position:absolute;left:500px; top:50px;text-align:justify;border:1px solid gray;padding:6px;-moz-border-radius:20px 0px 20px 20px;border-radius:20px 0px 20px 20px; background-color:#DDDDDD">
                Добредојдовте на системот за евиденција на редовност. <br />
                Доколку сте студент најавете се со вашето корисничко <br />
                име и лозинка. Доколку сте професор најавете се во <br />
                истата форма со вашето корисничко име и лозинка. Бла<br />
                блааа бла блаа бла бла.<br />
            </div>
        </div>

    </div>
    </form>
</body>
</html>
