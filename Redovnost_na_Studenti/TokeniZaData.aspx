﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TokeniZaData.aspx.cs" Inherits="Redovnost_na_Studenti.TokeniZaData" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Курсеви</title>
    <LINK href="Resources/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div align="center">
         <div class="headerDiv" align="center">
            <div class="headerInsideDiv">
                <img src="/Resources/Logo_FINKI_MK.jpg" style="height:50px;float:left"/>
                <div style="padding-top:14px;font-family: Arial, Helvetica, sans-serif; text-align:right;">
                    Најавен сте како: <asp:Label ID="Label1" runat="server" Text="" ForeColor="#0033CC"></asp:Label><!-- Ime Prezime -->
                    (<a href="Najava.aspx">Одјави се</a>)&nbsp&nbsp&nbsp&nbsp
                </div>
            </div>
        </div>

        

       <div class="bodyDiv">
        <br/>
            <a href="KurseviP.aspx" style=" text-decoration:none; cursor:pointer;"><asp:Label ID="Label5" class="labeli" runat="server" Text="Мои курсеви" style="float:left; margin-left:20px;" ForeColor="#2E92D0"></asp:Label></a>
            <asp:Image ID="Image1" runat="server" ImageUrl="~/Resources/triagolnik.png" Height="10px" Width="10px" style="float:left; margin-left:5px; margin-top:5px" />
            <a href="TokeniZaKurs.aspx" style=" text-decoration:none; cursor:pointer;"><asp:Label ID="Label2" runat="server" Text="" class="labeli" style="float:left; margin-left:5px;" ForeColor="#2E92D0" ></asp:Label></a>
            <asp:Image ID="Image2" runat="server" ImageUrl="~/Resources/triagolnik.png" Height="10px" Width="10px" style="float:left; margin-left:5px; margin-top:5px" />
            <asp:Label ID="Label3" runat="server" Text="" class="labeli" style="float:left; margin-left:5px;" ></asp:Label>
           
            <br/>
            <div style="float:left; margin-left:50px; margin-top:40px; ">
            <asp:GridView ID="GridView1" runat="server" DataSourceID="AccessDataSource1" 
                    EnableModelValidation="True" AutoGenerateColumns="False" Width="400px" Font-Size="18px" ForeColor="Black" 
                    HeaderStyle-BackColor="#2E92D0" RowStyle-Height="30px" HeaderStyle-Height="30px" BackColor="#DDDDDD">
                <Columns>
                    <asp:TemplateField HeaderText="Р.бр" >
                       <ItemTemplate>    
                           <%# ((GridViewRow)Container).RowIndex + 1%>
                       </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="id" SortExpression="id" Visible="False">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("id") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("id") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Токен" SortExpression="kluc">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("kluc") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("kluc") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Име" SortExpression="ime">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("ime") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("ime") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Презиме" SortExpression="prezime">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("prezime") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("prezime") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:ImageButton ID="ImageButton1" runat="server" Height="20px" 
                                ImageUrl="~/Resources/destroy.gif" onclick="ImageButton1_Click" 
                                Width="25px" TITLE="уништи токени" 
                                OnClientClick="return confirm('Дали сте сигурни дека сакате да ги уништите сите токени после овој, вклучувајќи го и него?')"/>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>

<HeaderStyle BackColor="#2E92D0"></HeaderStyle>

            </asp:GridView>
            <asp:AccessDataSource ID="AccessDataSource1" runat="server" 
                DataFile="App_Data/Redovnost.mdb" SelectCommand="
SELECT t.id, t.kluc, s.ime, s.prezime
FROM TOKEN t, STUDENT s 
WHERE t.data = sesData and
t.courseId=sesKursId and
t.studentId=s.id

UNION

SELECT t.id, t.kluc, ' ', ' '
FROM TOKEN t
WHERE t.data = sesData and
t.courseId=sesKursId and
t.kluc not in 
(SELECT t.kluc
FROM TOKEN t, STUDENT s 
WHERE t.data = sesData and
t.courseId=sesKursId and
t.studentId=s.id)

order by t.id" 
DeleteCommand="DELETE FROM TOKEN t
WHERE t.id &gt;= sesTokenId and
t.data = sesData and
t.courseId = sesKurs">
                <DeleteParameters>
                    <asp:SessionParameter Name="sesTokenId" SessionField="tokenId" />
                    <asp:SessionParameter Name="sesData" SessionField="izbranaData" />
                    <asp:SessionParameter Name="sesKurs" SessionField="kursParametar" />
                </DeleteParameters>
                <SelectParameters>
                    <asp:SessionParameter Name="sesData" SessionField="izbranaData" />
                    <asp:SessionParameter Name="sesKursId" SessionField="kursParametar" />
                </SelectParameters>
            </asp:AccessDataSource>
        </div>
    </div>
    </form>
</body>
</html>

