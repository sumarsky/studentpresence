﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Redovnost_na_Studenti
{
    public partial class KurseviS : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string imePrezime = Session["imePrezime"].ToString();
            Label1.Text = imePrezime;
            int idStudent = Convert.ToInt32(Session["id"].ToString());
        }



        protected void Button1_Click(object sender, EventArgs e)
        {
            GridViewRow gv = (GridViewRow)(((Control)sender).NamingContainer);
            Label lbid = (Label)gv.FindControl("Label2");
            String parametar = lbid.Text;
            Session["kursParametar"] = parametar;
            Response.Redirect("RedovnostS.aspx");
        }
    }
}