﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.OleDb;
using System.Configuration;
using System.Collections;

namespace Redovnost_na_Studenti
{
    public partial class Statistiki1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string imePrezime = Session["imePrezime"].ToString();
            Label1.Text = imePrezime;
            int idProfessor = Convert.ToInt32(Session["id"].ToString());

            string kursParametarId = Session["kursParametar"].ToString();

            string myConnection = ConfigurationManager.ConnectionStrings["konekcija"].ConnectionString;
            OleDbConnection conn = new OleDbConnection(myConnection);
            conn.Open();

            string imeKurs = "select kursIme from COURSE where ID = " + kursParametarId;
            OleDbCommand cmd1 = new OleDbCommand(imeKurs, conn);
            Object kIme = cmd1.ExecuteScalar();

            conn.Close();
            Label2.Text = kIme.ToString();
            Label3.Text = "Преглед на присуства за студенти";


           
            ArrayList grafData = new ArrayList();
            ArrayList grafStudenti = new ArrayList();
            ArrayList grafStudentiID = new ArrayList();

            conn.Open();
            string grafik = "Select DISTINCT data From Token WHERE courseId = " + kursParametarId;
            OleDbCommand cmd2 = new OleDbCommand(grafik, conn);

            OleDbDataReader r = cmd2.ExecuteReader();
            r.Read();
            if (r.HasRows)
            {
                DateTime tmp = DateTime.Parse(r["data"].ToString());
                String pom = tmp.ToString("dd.MM.yyyy");
                grafData.Add(pom.ToString());


                while (r.Read())
                {
                    DateTime tmp1 = DateTime.Parse(r["data"].ToString());
                    String pom1 = tmp1.ToString("dd.MM.yyyy");
                    grafData.Add(pom1.ToString());

                }
            }


            string studenti = "SELECT s.ID, s.ime, s.prezime, s.index FROM STUDENT s, STUDENT_COURSE k, COURSE c WHERE s.ID = k.studentId AND c.ID = k.courseID AND c.ID= " + kursParametarId;
            OleDbCommand cmd3 = new OleDbCommand(studenti, conn);
            OleDbDataReader r1 = cmd3.ExecuteReader();
            r1.Read();

            if (r1.HasRows)
            {
                grafStudentiID.Add(r1["ID"]);
                grafStudenti.Add(r1["ime"].ToString() + " " + r1["prezime"].ToString() + " " + r1["index"].ToString());
                while (r1.Read())
                {
                    grafStudentiID.Add(r1["ID"]);
                    grafStudenti.Add(r1["ime"].ToString() + " " + r1["prezime"].ToString() + " " + r1["index"].ToString());

                }
            }

            conn.Close();

            
            string javaScript = "<script type='text/javascript'> " +
                         "google.load('visualization', '1', {packages:['table']}); " +
                         "google.setOnLoadCallback(drawTable); " +
                         "function drawTable() { " +
                           "var data = new google.visualization.DataTable(); " +
                           "data.addColumn('string', 'Име'); ";
                           for(int i=0; i<grafData.Count; i++){
                               javaScript+="data.addColumn('boolean', '"+grafData[i]+"'); ";
                           }
                           javaScript += "data.addColumn('string', 'Присутност'); ";

                           javaScript += "data.addRows(" + grafStudenti.Count +"); ";

                           
                           for (int i = 0; i < grafStudenti.Count; i++ ){
                               int brojac = 0;
                               javaScript += "data.setCell(" + i + ",0, '" + grafStudenti[i] + "'); ";

                               for (int j = 1; j <= grafData.Count; j++)
                               {
                                   conn.Open();
                                   DateTime tmp5 = DateTime.Parse(grafData[j-1].ToString());
                                   String tmp6 = tmp5.ToString("dd.MM.yyyy");
                                   string pomos = "select s.ID from STUDENT s, TOKEN t where s.ID = t.studentId and t.data= ? AND s.ID= ? ";

                                   
                                   OleDbCommand cmd16 = new OleDbCommand(pomos, conn);
                                   cmd16.Parameters.AddWithValue("?", tmp6);
                                   cmd16.Parameters.AddWithValue("?", grafStudentiID[i]);
                                   Object ima = cmd16.ExecuteScalar();

                                   conn.Close();

                                   if (ima != null)
                                   {
                                       javaScript += "data.setCell(" + i + ", " + j + ", true); ";
                                       brojac++;
                                   }
                                   else
                                   {
                                       javaScript += "data.setCell(" + i + ", " + j + ", false); ";
                                   }
                                   
                               }

                               javaScript += "data.setCell(" + i + ","+(grafData.Count+1) +", '" + brojac.ToString()+" / "+ grafData.Count + "'); ";
                           }

                           javaScript += "var table = new google.visualization.Table(document.getElementById('dijagram')); " +
                           "table.draw(data, {showRowNumber: true}); " +
                         "} " +
                       "</script>";

            ClientScript.RegisterStartupScript(this.GetType(), "PageLoad", javaScript);

        }
    }
}