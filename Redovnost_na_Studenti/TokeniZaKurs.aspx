﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TokeniZaKurs.aspx.cs" Inherits="Redovnost_na_Studenti.TokeniZaKurs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Курсеви</title>
    <LINK href="Resources/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div align="center">
         <div class="headerDiv" align="center">
            <div class="headerInsideDiv">
                <img src="/Resources/Logo_FINKI_MK.jpg" style="height:50px;float:left"/>
                <div style="padding-top:14px;font-family: Arial, Helvetica, sans-serif; text-align:right;">
                    Најавен сте како: <asp:Label ID="Label1" runat="server" Text="" ForeColor="#0033CC"></asp:Label><!-- Ime Prezime -->
                    (<a href="Najava.aspx">Одјави се</a>)&nbsp&nbsp&nbsp&nbsp
                </div>
            </div>
        </div>



        <div class="bodyDiv">
        <br/>
            <a href="KurseviP.aspx" style=" text-decoration:none; cursor:pointer;"><asp:Label ID="Label5" class="labeli" runat="server" Text="Мои курсеви" style="float:left; margin-left:20px;" ForeColor="#2E92D0"></asp:Label></a>
            <asp:Image ID="Image1" runat="server" ImageUrl="~/Resources/triagolnik.png" Height="10px" Width="10px" style="float:left; margin-left:5px; margin-top:5px" />
            <asp:Label ID="Label2" runat="server" Text="" class="labeli" style="float:left; margin-left:5px;" ></asp:Label>
           
            <br/>
            <div style="float:left; margin-left:50px; margin-top:40px; ">
            <asp:GridView ID="GridView1" runat="server" DataSourceID="AccessDataSource1" 
                    EnableModelValidation="True" AutoGenerateColumns="False" Width="280px" 
                     Font-Size="18px" 
                    HeaderStyle-BackColor="#2E92D0" RowStyle-Height="30px" 
                    RowStyle-HorizontalAlign="Left" HeaderStyle-Height="30px" BackColor="#DDDDDD">
                <Columns>
                    <asp:TemplateField HeaderText="Датум" SortExpression="data" HeaderStyle-HorizontalAlign="NotSet">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("data") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("data") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:ImageButton ID="ImageButton1" runat="server" Height="20px" 
                                ImageUrl="~/Resources/destroy.gif" onclick="ImageButton1_Click" 
                                Width="25px" TITLE="уништи токени"/>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:ImageButton ID="ImageButton2" runat="server" Height="20px" 
                                ImageUrl="~/Resources/oko.jpg" onclick="ImageButton2_Click" 
                                Width="25px" TITLE="преглед на присутност за оваа дата" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:ImageButton ID="ImageButton3" runat="server" Height="20px" 
                                ImageUrl="~/Resources/delete.png" onclick="ImageButton3_Click" 
                                Width="25px" TITLE="избриши евиденција" 
                                OnClientClick="return confirm('Дали сте сигурни дека сакате да ја избришете евиденцијата за оваа дата?')"/>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:ImageButton ID="ImageButton6" runat="server" Height="20px" 
                                ImageUrl="~/Resources/print.png" onclick="ImageButton6_Click" Width="25px" TITLE="печати токени"/>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>

<HeaderStyle BackColor="#2E92D0"></HeaderStyle>

<RowStyle HorizontalAlign="Left" Height="30px"></RowStyle>

            </asp:GridView>
            <asp:AccessDataSource ID="AccessDataSource1" runat="server" 
                DataFile="App_Data/Redovnost.mdb" SelectCommand="SELECT data
                    FROM TOKEN t, COURSE c
                    where t.courseId=c.id and
                    c.id=kursId
                    group by data" DeleteCommand="DELETE FROM TOKEN t
                    WHERE t.data = sesData and t.courseId = sesKursId">
                <DeleteParameters>
                    <asp:SessionParameter Name="sesData" SessionField="izbranaData" />
                    <asp:SessionParameter Name="sesKursId" SessionField="kursParametar" />
                </DeleteParameters>
                <SelectParameters>
                    <asp:SessionParameter Name="kursId" SessionField="kursParametar" />
                </SelectParameters>
            </asp:AccessDataSource>
            <br/>

            <asp:TextBox ID="VnesiData" runat="server" Height="24px" Font-Bold="True" 
                    Font-Size="Medium" Width="100px"></asp:TextBox>
            &nbsp;&nbsp;
            <asp:Button ID="Generate" runat="server" Text="Генерирај токени" 
                    ValidationGroup="1" Font-Bold="True" Height="25px" Width="160px" 
                    onclick="Generate_Click" />
                <br/>
            <asp:RequiredFieldValidator ID="ValidirajData" runat="server" 
                    ErrorMessage="Внесете дата за која ќе се генерираат токени" 
                    ControlToValidate="VnesiData" ValidationGroup="1" Display="Dynamic"></asp:RequiredFieldValidator>

            <asp:RegularExpressionValidator ID="Format" runat="server" 
                    ErrorMessage="Внесете ја датата во формат DD.MM.YYYY" 
                    ControlToValidate="VnesiData" Display="Dynamic" 
                    ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[.](0[1-9]|1[012])[.](19|20)\d\d$" 
                    ValidationGroup="1"></asp:RegularExpressionValidator>
                <asp:AccessDataSource ID="AccessDataSource2" runat="server" 
                    DataFile="/App_Data/Redovnost.mdb" 
                    
                    
                    SelectCommand="SELECT COUNT(*) AS Expr1 FROM STUDENT_COURSE WHERE (courseId = paramCid)" 
                    
                    InsertCommand="INSERT INTO TOKEN(courseId, kluc, data) VALUES (paraCid, paraKluc, paraData)">
                    <InsertParameters>
                        <asp:SessionParameter Name="paraCid" SessionField="kursParametar" />
                        <asp:SessionParameter Name="paraKluc" SessionField="token" />
                        <asp:FormParameter FormField="VnesiData" Name="paraData" />
                    </InsertParameters>
                    <SelectParameters>
                        <asp:SessionParameter Name="paramCid" SessionField="kursParametar" />
                    </SelectParameters>
                </asp:AccessDataSource>
        </div>

        <div id="oko" style="float:right; margin-right:300px; margin-top:40px; ">
            <asp:ImageButton ID="ImageButton5" runat="server" 
                ImageUrl="~/Resources/table.png" Height="50px" Width="50px" 
                TITLE="Преглед на присуства на студенти за овој курс" onclick="ImageButton5_Click" style="margin-left:10px;"/><br/><br/>
            &nbsp;
            <asp:ImageButton ID="ImageButton4" runat="server" 
                ImageUrl="~/Resources/grid.png" Height="50px" Width="50px" 
                TITLE="Статистички податоци за присуство за курсот" onclick="ImageButton4_Click"/>
            
        </div>
            
    </div>
    </form>
</body>
</html>
